## Table of contents
* [Contributors](#contributors)
* [General info](#general-info)
* [Technologies](#technologies)
* [Requirement](#requirement)
* [Setup](#setup)
* [Get the web page](#get-the-web-page)
* [Test](#test)
* [Issues encountered](#issues-encountered)

## Contributors
HUSSONNOIS Maxence and EVAN-MIALON Remi (ING4 SI Gr03)

## General info
This project is a school project for Node and DevOps course.

For Node project we had to do :
* API side 
  - CRUD users 
  - Authenticate
  - CRUD your own metrics (make use of an authorization middleware)
* Front side
  - Home page
  - Sign In / Sign Up / Sign Out
  - Insert/update/delete metrics once logged in
  - Retrieve the user’s metrics and display it in a graph
  - Only access the user’s metrics, not the other ones
* Utils 
  - pre-populate the database with at least two users and their own metrics

For DevOps project we had to do :
- Use git, use branches, use tags
- Your project must have tests with a testing framework (depending on the language used)
- If you project uses external dependencies such as a database, use docker-compose to start it before the test session.
- Configure your project to use a CI server
- Provide a "README" file explaining how to launch your project, how to launch the tests, what problems your had
	
## Technologies
Project is created with:
* Docker-compose 
* Node
* npm
* Mongoose
* Mocha

## Requirement
Docker and docker-compose

## Setup
To run this project, install it locally using docker-compose:

```
$ cd ./app
$ docker-compose build
$ docker-compose up -d
```
## Get the web page
The app will be available on localhost:

```
$ http://localhost:83/
```

## Test
To run test local run the the following command, otherwise you can use the pipelines on gitlab project:

```
$ docker exec -it Ece-docker-node-mongo npm run test
```


## Issues encountered
For the DevOps project the issues was to setup the environment and to launch the tests using the CI/CD server
For the Node project the issues was used a new dataBase to me which was MongoDB and mongoose.