var mongoose = require('mongoose');
var Schema = mongoose.Schema;



const MetricSchema = new Schema({
  timestamp: Number,
  value : Number,
  userId: String
  
});

var Metric = mongoose.model('metric', MetricSchema);
module.exports =  Metric
