import { rejects } from "assert";
import { resolve } from "dns";


const mongoose = require('mongoose');
mongoose.Promise = global.Promise
const DB_URI ='mongodb://mongo:27017/userMetrics'
/*
function connect(){
    return new Promise((resolve, reject) => {
        mongoose.connect(DB_URI,
            {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true  })
            .then((res,err)=>{
                if(err) return rejects(err);
                resolve();
            })
            .catch(e => console.log("DB error", e));
    });

}
*/
const connect = async () => {
    mongoose.connect('mongodb://mongo:27017/userMetrics', {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true})
    console.log('DB Connected....');
    }
const close = async() => {
    mongoose.disconnect();
    console.log('DB Disconnected....');
}

module.exports = {connect, close}