var assert = require('assert');

var Metric= require('../models/metrics');
var db = require ('../database.ts');



describe('Creating metrics', function()  {
    before((done)=>{
        db.connect()
        .then(()=> done())
    })
    it('creates a metrics', (done) => {
        //assertion is not included in mocha so 
        //require assert which was installed along with mocha
        const newMetric = new Metric({
            timestamp : 12321323213,
            value : 23,
            userId : '1'
          });
          newMetric.save() //takes some time and returns a promise
            .then(() => {
                assert(!newMetric.isNew); 
                done();
            });
    });
    after((done)=>{
        db.close()
        .then(()=> done())
    })
});