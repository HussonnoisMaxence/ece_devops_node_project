import { resolve } from "dns";

var assert = require('assert');

var Metric= require('../models/metrics');
var db = require ('../database.ts');



describe('Find and delete a metrics', function()  {
    var newMetric
    var newMetric2
    before((done)=>{
        db.connect()
        .then(()=> done())

    })
        it('finds metric value by its userId', (done) => {
           newMetric = new Metric({
                timestamp : 12321323213,
                value : 22,
                userId : '1'
              }),
            newMetric.save() 
            Metric.findOne({ userId: '1' })
                .then((metrics) => {
                    assert(metrics.userId === '1'); 
                    done();
                });
        })
        
        it('delete a matrics by its id', async() => {
            newMetric2 = await new Metric({
                timestamp : 12321323213,
                value : 23,
                userId : '2'
              }),
             await newMetric2.save() 
              var id = newMetric2._id
             await Metric.findByIdAndDelete(newMetric2._id)
             .then(async() => await Metric.findOne({ _id: id }))
             .then((metric) => { 
              assert(metric === null);;
            });
        })
        
    after((done)=>{
        db.close()
        .then(()=> done())
    })
});