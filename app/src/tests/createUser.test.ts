var assert = require('assert');

var User= require('../models/user');
var db = require ('../database.ts');



describe('Creating user', function()  {
    before((done)=>{
        db.connect()
        .then(()=> done())
    })
    it('creates a user', (done) => {
        //assertion is not included in mocha so 
        //require assert which was installed along with mocha
        const newUser = new User({
            username: 'MaxLaMenace',
            email: 'futurLeader@gmail.com',
            password: 'loveCats'
          });
          newUser.save() //takes some time and returns a promise
            .then(() => {
                assert(!newUser.isNew); 
                done();
            });
    });
    after((done)=>{
        db.close()
        .then(()=> done())
    })
});