import bodyparser from 'body-parser'
import session = require('express-session')
var MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');
mongoose.Promise = global.Promise
var path = require('path')
var express = require("express");
var app = express();
var connectedUser= require('./models/user');
app.set('views', __dirname + "/views")
app.set('view engine', 'ejs');
app.set('views', __dirname + "/views")
app.set('view engine', 'ejs');
app.use(bodyparser.json())
app.use(bodyparser.urlencoded())
app.use(express.static(path.join(__dirname, 'user')))
app.use(express.static(path.join(__dirname, 'views')))
const userRouter = express.Router()

app.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: mongoose.connection,
    collection: 'sessions'
  })
  })
);



// USER METRICS

const Metric= require('./models/metrics');
// Get metrics from the db
userRouter.get('/user',  requiresLogin,(req: any, res: any) => {
  User.find({ _id : req.session.userId})
  .then(user => connectedUser=user[0], Metric.find({userId : req.session.userId})
  .then(metrics => 
   res.render('user.ejs', { metrics, connectedUser }))
  .catch(err => res.status(404).json({ msg: 'No items found' }))
 )
})
  
 userRouter.get('/metrics',  requiresLogin,async(req: any, res: any) => {  
  var momo= {}
  var userM = new Array()
  await Metric.find({userId : req.session.userId})
  .then(metrics => res.send(metrics))
   .catch(err => res.status(404).json({ msg: 'No items found' }));
 })

// Add metrics to the db
userRouter.post('/metrics', requiresLogin, (req, res) => {  
  const newMetric = new Metric({
    timestamp : req.body.timestamp,
    value : Math.floor(Math.random() * 30),
    userId : req.session.userId
  });

  newMetric.save().then(metric => res.redirect('/user'));
});
// Update value of metrics
userRouter.patch('/metrics', requiresLogin, (req, res) => {
  Metric.update({_id: req.body.id}, {
   value : Math.floor(Math.random() * 30)
  },function (err) {
    if(err) console.log(err);
    console.log("Successful update");
    res.send('ok')
  });
});

//Delete metrics from the db
userRouter.delete('/metrics', requiresLogin, (req, res) => {
  Metric.findByIdAndDelete(req.body.id, function (err) {
    if(err) console.log(err);
    console.log("Successful deletion");
    res.send('ok')
  });
});

//

app.use(userRouter)
// Authentification metrics

const User= require('./models/user');
const authRouter = express.Router()
authRouter.get('/', (req: any, res: any,) => {
  res.render('login.ejs')
})
authRouter.post('/', (req: any, res: any, next:any) => {
  if (req.body.email && req.body.password) {
    User.authenticate(req.body.email, req.body.password, function (error, user) {
      if (error || !user) {
        var err = new Error('Wrong email or password.');
        return next(err);
      } else {
        req.session.userId = user._id;
        return res.redirect('/user');
      }
    });
  } else {
    var err = new Error('All fields required.');
    return next(err);
  }
})

authRouter.get('/logout', function(req, res, next) {
  if (req.session) {
    // delete session object
    req.session.destroy(function(err) {
      if(err) {
        return next(err);
      } else {
        return res.redirect('/');
      }
    });
  }
});


// Sign up
authRouter.get('/signUp', (req: any, res: any) => {
   res.render('signUp.ejs')
 })
 authRouter.post('/signup', (req, res) => {
  const newUser = new User({
    username: req.body.username,
    email: req.body.email,
    password: req.body.password
  });

  newUser.save().then(user => res.redirect('/'));

});

app.use(authRouter)

function requiresLogin(req, res, next) {
  if (req.session && req.session.userId) {
    return next();
  } else {
    var err = new Error('You must be logged in to view this page.');
    return next(err);
  }
}


module.exports = app;